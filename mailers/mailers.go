package mailers

import (
	"log"

	"github.com/gobuffalo/buffalo/mail"
	"github.com/gobuffalo/buffalo/render"
	"github.com/gobuffalo/envy"
	"github.com/gobuffalo/packr"
	"github.com/stanislas-m/mocksmtp"
)

// SMTP is the mail sender used to send mails.
// When GO_ENV == "test", this is a mock implementation using github.com/stanislas-m/mocksmtp
var SMTP mail.Sender
var r *render.Engine

func init() {

	if envy.Get("GO_ENV", "development") == "test" {
		SMTP = mocksmtp.New()
	} else {
		// Pulling config from the env.
		port := envy.Get("SMTP_PORT", "")
		host := envy.Get("SMTP_HOST", "")
		user := envy.Get("SMTP_USER", "")
		password := envy.Get("SMTP_PASSWORD", "")

		var err error
		SMTP, err = mail.NewSMTPSender(host, port, user, password)

		if err != nil {
			log.Fatal(err)
		}
	}

	r = render.New(render.Options{
		HTMLLayout:   "layout.html",
		TemplatesBox: packr.NewBox("../templates/mail"),
		Helpers:      render.Helpers{},
	})
}
