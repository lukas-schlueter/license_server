package mailers

import (
	"github.com/gobuffalo/buffalo"
	"github.com/gobuffalo/buffalo/mail"
	"github.com/gobuffalo/envy"
	"github.com/pkg/errors"
)

// SendLoginLink sends the confirmation link in an email to to.
func SendLoginLink(c buffalo.Context) error {
	m := mail.New(c)

	// fill in with your stuff:
	m.Subject = "Login Link"
	m.From = envy.Get("MAIL_SENDER", "")
	m.To = []string{c.Data()["to"].(string)}

	data := map[string]interface{}{
		"host":   c.Data()["host"],
		"userID": c.Data()["userID"],
		"token":  c.Data()["token"],
	}

	err := m.AddBody(r.HTML("login_link.html"), data)
	if err != nil {
		return errors.WithStack(err)
	}
	return SMTP.Send(m)
}
