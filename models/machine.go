package models

import (
	"encoding/json"
	"time"

	"github.com/gobuffalo/pop"
	"github.com/gobuffalo/uuid"
	"github.com/gobuffalo/validate"
)

// Machine represents an installation instance of software.
type Machine struct {
	ID        uuid.UUID `json:"id" db:"id"`
	CreatedAt time.Time `json:"created_at" db:"created_at"`
	UpdatedAt time.Time `json:"updated_at" db:"updated_at"`
	User      User      `json:"user" db:"-" belongs_to:"user" form:"-"`
	UserID    uuid.UUID `json:"user_id" db:"user_id" form:"-"`
	Licenses  Licenses  `json:"licenses" db:"-" many_to_many:"machines_licenses" form:"-"`
}

// String is not required by pop and may be deleted
func (m Machine) String() string {
	jm, _ := json.Marshal(m)
	return string(jm)
}

// Machines is not required by pop and may be deleted
type Machines []Machine

// String is not required by pop and may be deleted
func (m Machines) String() string {
	jm, _ := json.Marshal(m)
	return string(jm)
}

// Validate gets run every time you call a "pop.Validate*" (pop.ValidateAndSave, pop.ValidateAndCreate, pop.ValidateAndUpdate) method.
// This method is not required and may be deleted.
func (m *Machine) Validate(tx *pop.Connection) (*validate.Errors, error) {
	return validate.NewErrors(), nil
}

// ValidateCreate gets run every time you call "pop.ValidateAndCreate" method.
// This method is not required and may be deleted.
func (m *Machine) ValidateCreate(tx *pop.Connection) (*validate.Errors, error) {
	return validate.NewErrors(), nil
}

// ValidateUpdate gets run every time you call "pop.ValidateAndUpdate" method.
// This method is not required and may be deleted.
func (m *Machine) ValidateUpdate(tx *pop.Connection) (*validate.Errors, error) {
	return validate.NewErrors(), nil
}
