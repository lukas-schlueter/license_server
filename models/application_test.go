package models

import (
	"github.com/gobuffalo/uuid"
)

func (ms *ModelSuite) Test_Application_Create() {
	ms.LoadFixture("no applications")
	u := &User{}
	err := ms.DB.First(u)
	ms.NoError(err)

	count, err := ms.DB.Count("applications")
	ms.NoError(err)
	ms.Equal(0, count)

	a := &Application{
		Name:   "TestApp",
		UserID: u.ID,
	}
	ms.Equal(uuid.Nil, a.ID)

	verrs, err := ms.DB.ValidateAndCreate(a)
	ms.NoError(err)
	ms.False(verrs.HasAny())
	ms.NotEqual(uuid.Nil, a.ID, "DB failed to set ID")

	count, err = ms.DB.Count("applications")
	ms.NoError(err)
	ms.Equal(1, count)
}
