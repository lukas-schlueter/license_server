package models

import (
	"fmt"

	"github.com/gobuffalo/uuid"
)

func (ms *ModelSuite) Test_Token_Create() {
	count, err := ms.DB.Count("tokens")
	ms.NoError(err)
	ms.Equal(0, count)

	user := &User{Email: "max@example.com"}
	verrs, err := ms.DB.ValidateAndCreate(user)
	ms.NoError(err)
	ms.False(verrs.HasAny())

	token := &Token{UserID: user.ID}
	verrs, err = ms.DB.ValidateAndCreate(token)
	ms.NoError(err)
	ms.False(verrs.HasAny())

	count, err = ms.DB.Count("tokens")
	ms.NoError(err)
	ms.Equal(1, count)
}

func (ms *ModelSuite) Test_Token_Create_NoUserID() {
	count, err := ms.DB.Count("tokens")
	ms.NoError(err)
	ms.Equal(0, count)

	token := &Token{}
	verrs, err := ms.DB.ValidateAndCreate(token)
	ms.NoError(err)
	ms.True(verrs.HasAny())
	ms.Equal(1, verrs.Count())
	ms.Equal("Invalid UserID: 00000000-0000-0000-0000-000000000000", verrs.Get("user_id")[0])

	count, err = ms.DB.Count("tokens")
	ms.NoError(err)
	ms.Equal(0, count)
}

func (ms *ModelSuite) Test_Token_Create_InvalidUserID() {
	count, err := ms.DB.Count("tokens")
	ms.NoError(err)
	ms.Equal(0, count)

	id := uuid.Must(uuid.NewV4())
	token := &Token{
		UserID: id,
	}
	verrs, err := ms.DB.ValidateAndCreate(token)
	ms.NoError(err)
	ms.True(verrs.HasAny())
	ms.Equal(1, verrs.Count())
	ms.Equal(fmt.Sprintf("Invalid UserID: %s", id.String()), verrs.Get("user_id")[0])

	count, err = ms.DB.Count("tokens")
	ms.NoError(err)
	ms.Equal(0, count)
}
