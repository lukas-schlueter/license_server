package models

import (
	"encoding/json"
	"time"

	"github.com/gobuffalo/pop"
	"github.com/gobuffalo/uuid"
	"github.com/gobuffalo/validate"
	"github.com/gobuffalo/validate/validators"
)

// License provides access to an application for a limited amount of machines.
type License struct {
	ID            uuid.UUID   `json:"id" db:"id" form:"-"`
	CreatedAt     time.Time   `json:"created_at" db:"created_at" form:"-"`
	UpdatedAt     time.Time   `json:"updated_at" db:"updated_at" form:"-"`
	MachineLimit  int         `json:"machine_limit" db:"machine_limit"`
	User          User        `json:"user" db:"-" belongs_to:"user" form:"-"`
	UserID        uuid.UUID   `json:"user_id" db:"user_id" form:"-"`
	Application   Application `json:"application" db:"-" belongs_to:"application" form:"-"`
	ApplicationID uuid.UUID   `json:"application_id" db:"application_id" form:"-"`
	Machines      Machines    `json:"machines" db:"-" many_to_many:"machines_licenses" form:"-"`
}

// String is not required by pop and may be deleted
func (l License) String() string {
	jl, _ := json.Marshal(l)
	return string(jl)
}

// Licenses is not required by pop and may be deleted
type Licenses []License

// String is not required by pop and may be deleted
func (l Licenses) String() string {
	jl, _ := json.Marshal(l)
	return string(jl)
}

// Validate gets run every time you call a "pop.Validate*" (pop.ValidateAndSave, pop.ValidateAndCreate, pop.ValidateAndUpdate) method.
// This method is not required and may be deleted.
func (l *License) Validate(tx *pop.Connection) (*validate.Errors, error) {
	return validate.Validate(
		&validators.IntIsPresent{Field: l.MachineLimit, Name: "MachineLimit"},
		&validators.UUIDIsPresent{Field: l.UserID, Name: "UserID"},
		&validators.UUIDIsPresent{Field: l.ApplicationID, Name: "ApplicationID"},
	), nil
}

// ValidateUpdate gets run every time you call "pop.ValidateAndUpdate" method.
// This method is not required and may be deleted.
func (l *License) ValidateUpdate(tx *pop.Connection) (*validate.Errors, error) {
	return validate.Validate(
		&validators.UUIDIsPresent{Field: l.ID, Name: "ID"},
	), nil
}
