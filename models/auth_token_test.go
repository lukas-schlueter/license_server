package models

import (
	"github.com/gofrs/uuid"
)

func (ms *ModelSuite) Test_AuthToken() {
	id, err := uuid.NewV4()
	ms.NoError(err)

	token1 := &AuthToken{
		ID:    id,
		Nonce: 100,
	}

	verrs, err := ms.DB.ValidateAndCreate(token1)
	ms.False(verrs.HasAny())
	ms.NoError(err)

	token2 := &AuthToken{
		ID:    id,
		Nonce: 100,
	}
	verrs, err = ms.DB.ValidateAndCreate(token2)
	ms.False(verrs.HasAny())
	ms.Errorf(err, "AuthToken ids should be unique!")
}
