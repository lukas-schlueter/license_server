package models

import (
	"github.com/gobuffalo/uuid"
)

func (ms *ModelSuite) Test_License_Create() {
	ms.LoadFixture("no licenses")
	u := &User{}
	err := ms.DB.First(u)
	ms.NoError(err)

	a := &Application{}
	err = ms.DB.First(a)
	ms.NoError(err)

	count, err := ms.DB.Count("licenses")
	ms.NoError(err)
	ms.Equal(0, count)

	l := &License{
		MachineLimit:  5,
		UserID:        u.ID,
		ApplicationID: a.ID,
	}
	ms.Equal(uuid.Nil, l.ID)

	verrs, err := ms.DB.ValidateAndCreate(l)
	ms.NoError(err)
	ms.False(verrs.HasAny())
	ms.NotEqual(uuid.Nil, l.ID, "DB failed to set ID")

	count, err = ms.DB.Count("licenses")
	ms.NoError(err)
	ms.Equal(1, count)
}
