package models

import (
	"github.com/gobuffalo/uuid"
)

func (ms *ModelSuite) Test_User_Create() {
	count, err := ms.DB.Count("users")
	ms.NoError(err)
	ms.Equal(0, count)

	u := &User{
		Email: "max@example.com",
	}
	ms.Equal(uuid.Nil, u.ID)

	verrs, err := ms.DB.ValidateAndCreate(u)
	ms.NoError(err)
	ms.False(verrs.HasAny())
	ms.NotEqual(uuid.Nil, u.ID, "DB failed to set ID")

	count, err = ms.DB.Count("users")
	ms.NoError(err)
	ms.Equal(1, count)
}

func (ms *ModelSuite) Test_User_Create_InvalidEmail() {
	count, err := ms.DB.Count("users")
	ms.NoError(err)
	ms.Equal(0, count)

	u := &User{
		Email: "not.an.email.com",
	}
	ms.Equal(uuid.Nil, u.ID)

	verrs, err := ms.DB.ValidateAndCreate(u)
	ms.NoError(err)
	ms.True(verrs.HasAny())
	ms.Equal(1, verrs.Count())
	ms.Equal("Email does not match the email format.", verrs.Get("email")[0])

	count, err = ms.DB.Count("users")
	ms.NoError(err)
	ms.Equal(0, count)
}

func (ms *ModelSuite) Test_User_Create_UserExists() {
	count, err := ms.DB.Count("users")
	ms.NoError(err)
	ms.Equal(0, count)

	user1 := &User{
		Email: "max@example.com",
	}
	ms.Equal(uuid.Nil, user1.ID)

	verrs, err := ms.DB.ValidateAndCreate(user1)
	ms.NoError(err)
	ms.False(verrs.HasAny())
	ms.NotEqual(uuid.Nil, user1.ID, "DB failed to set ID")

	count, err = ms.DB.Count("users")
	ms.NoError(err)
	ms.Equal(1, count)

	user2 := &User{
		Email: "max@example.com",
	}
	verrs, err = ms.DB.ValidateAndCreate(user2)
	ms.NoError(err)
	ms.True(verrs.HasAny())
	ms.Equal(1, verrs.Count())
	ms.Equal("max@example.com is already taken", verrs.Get("email")[0])

	count, err = ms.DB.Count("users")
	ms.NoError(err)
	ms.Equal(1, count)

	user3 := &User{
		Email: "not.max@example.com",
		ID:    user1.ID,
	}
	verrs, err = ms.DB.ValidateAndCreate(user3)
	ms.NoError(err)
	ms.True(verrs.HasAny())
	ms.Equal(1, verrs.Count())

	count, err = ms.DB.Count("users")
	ms.NoError(err)
	ms.Equal(1, count)
}
