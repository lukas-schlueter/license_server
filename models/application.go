package models

import (
	"encoding/json"
	"time"

	"github.com/gobuffalo/pop"
	"github.com/gobuffalo/uuid"
	"github.com/gobuffalo/validate"
	"github.com/gobuffalo/validate/validators"
)

// Application represents a user defined structure that can be updated.
// Access to an application is limited by Licenses.
type Application struct {
	ID        uuid.UUID `json:"id" db:"id"`
	CreatedAt time.Time `json:"created_at" db:"created_at"`
	UpdatedAt time.Time `json:"updated_at" db:"updated_at"`
	UserID    uuid.UUID `json:"user_id" db:"user_id"`
	Licenses  Licenses  `json:"licenses" db:"-" has_many:"licenses"`
	Name      string    `json:"name" db:"name"`
}

// String is not required by pop and may be deleted
func (a Application) String() string {
	ja, _ := json.Marshal(a)
	return string(ja)
}

// Applications is not required by pop and may be deleted
type Applications []Application

// String is not required by pop and may be deleted
func (a Applications) String() string {
	ja, _ := json.Marshal(a)
	return string(ja)
}

// Validate gets run every time you call a "pop.Validate*" (pop.ValidateAndSave, pop.ValidateAndCreate, pop.ValidateAndUpdate) method.
// This method is not required and may be deleted.
func (a *Application) Validate(tx *pop.Connection) (*validate.Errors, error) {
	return validate.Validate(
		&validators.StringIsPresent{Field: a.Name, Name: "Name"},
		&validators.UUIDIsPresent{Field: a.UserID, Name: "UserID"},
	), nil
}

// ValidateUpdate gets run every time you call "pop.ValidateAndUpdate" method.
// This method is not required and may be deleted.
func (a *Application) ValidateUpdate(tx *pop.Connection) (*validate.Errors, error) {
	return validate.Validate(
		&validators.UUIDIsPresent{Field: a.ID, Name: "ID"},
	), nil
}
