package models

import (
	"encoding/json"
	"time"

	"github.com/gobuffalo/pop"
	"github.com/gobuffalo/uuid"
	"github.com/gobuffalo/validate"
	"github.com/gobuffalo/validate/validators"
)

// Token represents a confirmation token used to confirm/login users.
type Token struct {
	ID        uuid.UUID `json:"id" db:"id"`
	CreatedAt time.Time `json:"created_at" db:"created_at"`
	UpdatedAt time.Time `json:"updated_at" db:"updated_at"`
	User      User      `json:"user" db:"-" belongs_to:"user"`
	UserID    uuid.UUID `json:"user_id" db:"user_id"`
}

// String is not required by pop and may be deleted
func (t Token) String() string {
	jt, _ := json.Marshal(t)
	return string(jt)
}

// Tokens is not required by pop and may be deleted
type Tokens []Token

// String is not required by pop and may be deleted
func (t Tokens) String() string {
	jt, _ := json.Marshal(t)
	return string(jt)
}

// Validate gets run every time you call a "pop.Validate*" (pop.ValidateAndSave, pop.ValidateAndCreate, pop.ValidateAndUpdate) method.
// This method is not required and may be deleted.
func (t *Token) Validate(tx *pop.Connection) (*validate.Errors, error) {
	return validate.NewErrors(), nil
}

// ValidateCreate gets run every time you call "pop.ValidateAndCreate" method.
// This method is not required and may be deleted.
func (t *Token) ValidateCreate(tx *pop.Connection) (*validate.Errors, error) {
	var err error
	return validate.Validate(
		&validators.FuncValidator{
			Field:   t.UserID.String(),
			Name:    "UserID",
			Message: "Invalid UserID: %s",
			Fn: func() bool {
				if t.UserID == uuid.Nil {
					return false
				}
				if err = tx.Find(&User{}, t.UserID); err != nil {
					return false
				}
				return true
			},
		},
	), nil
}
