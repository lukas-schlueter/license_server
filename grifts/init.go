package grifts

import (
	"github.com/gobuffalo/buffalo"
	"gitlab.com/lukas-schlueter/license_server/actions"
)

func init() {
	buffalo.Grifts(actions.App())
}
