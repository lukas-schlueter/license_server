package api

import (
	"github.com/gobuffalo/buffalo"
)

// SampleHandler is a dummy handler to test API authentication.
func SampleHandler(c buffalo.Context) error {
	if features.IsEnabled("api-sample-json") {
		return c.Render(200, r.JSON(sample{Msg: "TestMsg"}))
	}
	return c.Render(200, r.String("Hello from api!"))
}

type sample struct {
	Msg   string `json:"msg"`
	Dummy string `json:"dummy"`
}
