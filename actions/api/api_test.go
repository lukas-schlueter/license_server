package api

import (
	"fmt"
	"log"
	"math/rand"
	"strconv"
	"testing"
	"time"

	"github.com/gobuffalo/httptest"
	"github.com/gobuffalo/packr"
	"github.com/gobuffalo/suite"
	"github.com/gofrs/uuid"
	"github.com/o1egl/paseto"
	. "gitlab.com/lukas-schlueter/license_server/features"
	"gitlab.com/lukas-schlueter/license_server/models"
)

type ActionSuite struct {
	*suite.Action
}

var featureMock *MockProvider

func init() {
	featureMock = &MockProvider{}
	features = featureMock
	err := features.Initialize()

	if err != nil {
		log.Fatal(err)
	}
}

func Test_ActionSuite(t *testing.T) {
	action, err := suite.NewActionWithFixtures(App(), packr.NewBox("../../fixtures/api"))

	if err != nil {
		t.Fatal(err)
	}

	as := &ActionSuite{
		Action: action,
	}
	suite.Run(t, as)
}

func require(features []string, context func()) {
	for _, feature := range features {
		featureMock.SetEnabled(feature, true)
	}
	defer func() {
		for _, feature := range features {
			featureMock.SetEnabled(feature, false)
		}
	}()
	context()
}

func get(request *httptest.JSON, token string, nonce int) (*httptest.JSONResponse, int) {
	request.Headers["Authorization"] = "Bearer " + token
	request.Headers["X-Content-Nonce"] = strconv.Itoa(nonce)
	res := request.Get()
	newNonce, _ := strconv.Atoi(res.Header().Get("X-Content-Nonce"))
	return res, newNonce
}

type testTokenOptions struct {
	id           *uuid.UUID
	data         *map[string]string
	footer       *string
	issuedAt     *time.Time
	notBefore    *time.Time
	expiresAfter *time.Duration
	issuer       *string
	audience     *string
	subject      *string
}

func (as *ActionSuite) getTestToken(options testTokenOptions) (string, int) {
	authKey = []byte("TEST-KEY-TEST-KEY-TEST-KEY-TEST-")

	if options.id == nil {
		id, err := uuid.NewV4()
		as.NoError(err)
		options.id = &id
	}

	nonce := rand.Intn(900000000) + 100000000
	authToken := &models.AuthToken{
		ID:    *options.id,
		Nonce: nonce,
	}
	verrs, err := as.DB.ValidateAndCreate(authToken)
	as.NoError(err)
	as.False(verrs.HasAny())

	t := &paseto.JSONToken{
		Jti:        (*options.id).String(),
		IssuedAt:   timeOr(options.issuedAt, authToken.CreatedAt),
		NotBefore:  timeOr(options.notBefore, authToken.CreatedAt),
		Expiration: timeOr(options.issuedAt, authToken.CreatedAt).Add(durationOr(options.expiresAfter, 10*time.Minute)),
		Issuer:     stringOr(options.issuer, ""),
		Subject:    stringOr(options.subject, ""),
		Audience:   stringOr(options.audience, ""),
	}
	if options.data != nil {
		for key, value := range *options.data {
			t.Set(key, value)
		}
	}
	fmt.Printf("token is %s", t)

	var token string
	token, err = v2.Encrypt(authKey, t, stringOr(options.footer, ""))

	as.NoError(err)
	return token, nonce
}

func stringOr(nullable *string, or string) string {
	if nullable == nil {
		return or
	}
	return *nullable
}
func durationOr(nullable *time.Duration, or time.Duration) time.Duration {
	if nullable == nil {
		return or
	}
	return *nullable
}

func timeOr(nullable *time.Time, or time.Time) time.Time {
	if nullable == nil {
		return or
	}
	return *nullable
}
