package api

import (
	"log"
	"math/rand"
	"net/http"
	"strconv"
	"strings"
	"time"

	"github.com/gobuffalo/buffalo"
	"github.com/gobuffalo/pop"
	"github.com/gofrs/uuid"
	"github.com/o1egl/paseto"
	"github.com/pkg/errors"
	"gitlab.com/lukas-schlueter/license_server/models"
)

// Options provide a way to pass arguments to the paseto middleware.
type Options struct {
	Key []byte
}

func getTokenFromHeader(authString string) (string, error) {
	if authString == "" {
		return "", ErrNoToken
	}
	authString = strings.TrimSpace(authString)
	if !strings.HasPrefix(authString, "Bearer ") {
		return "", ErrTokenInvalid
	}
	return strings.TrimPrefix(authString, "Bearer "), nil
}

// AuthenticateToken validates a PASETO token passed in the Authorization HTTP header with type 'Bearer'.
// Token jti needs to be stored as AuthToken in the db to be valid.
func AuthenticateToken(options Options) buffalo.MiddlewareFunc {
	if options.Key == nil {
		log.Fatal(errors.New("paseto key not found"))
	}
	authKey = options.Key

	return func(next buffalo.Handler) buffalo.Handler {
		return func(c buffalo.Context) error {
			tx, ok := c.Value("tx").(*pop.Connection)
			if !ok {
				return errors.WithStack(errors.New("no transaction found"))
			}

			authString := c.Request().Header.Get("Authorization")

			tokenString, err := getTokenFromHeader(authString)

			if err != nil {
				return c.Error(http.StatusUnauthorized, err)
			}

			var token paseto.JSONToken
			var footer string
			err = v2.Decrypt(tokenString, authKey, &token, &footer)

			if err != nil {
				return c.Error(http.StatusUnauthorized, err)
			}

			buffalo.NewLogger("debug").Debugf("Token is %v - footer is '%s'", token, footer)

			authToken := &models.AuthToken{}

			if err := tx.Find(authToken, uuid.FromStringOrNil(token.Jti)); err != nil {
				return c.Error(http.StatusUnauthorized, ErrTokenInvalid)
			}

			// By default, pop drops the time zone for the database and stores the local time (not utc)
			// The paseto token preserves the time zone.
			// The following tries to calculate the correct time for db item.
			// NOTE: If the current local time differs from the local time (of the db?) at creation, this will fail!
			// TODO: Make this more robust, maybe there will be a fix for pop?
			_, tz := time.Now().Zone()
			realUnix := authToken.CreatedAt.Add(-time.Second * time.Duration(tz)).Unix()

			if authToken.ID != uuid.FromStringOrNil(token.Jti) || realUnix != token.IssuedAt.Unix() {
				return c.Error(http.StatusUnauthorized, ErrTokenInvalid)
			}
			tokenJSON, err := token.MarshalJSON()
			c.Set("paseto_token", string(tokenJSON))
			return next(c)
		}
	}
}

func getTokenFromContext(c buffalo.Context) (paseto.JSONToken, error) {
	tokenJson := c.Data()["paseto_token"].(string)
	token := paseto.JSONToken{}
	err := token.UnmarshalJSON([]byte(tokenJson))
	return token, err
}

// AuthenticateNonce checks if the nonce provided in the request matches the stored nonce.
// WARNING: Deletes token if the nonces mismatch.
func AuthenticateNonce(next buffalo.Handler) buffalo.Handler {
	return func(c buffalo.Context) error {
		tx, ok := c.Value("tx").(*pop.Connection)
		if !ok {
			return errors.WithStack(errors.New("no transaction found"))
		}
		nonce := c.Request().Header.Get("X-Content-Nonce")
		token, err := getTokenFromContext(c)
		if err != nil {
			return c.Render(http.StatusInternalServerError, r.Auto(c, Error{"Internal server error!"}))
		}
		tokenID := uuid.FromStringOrNil(token.Jti)

		authToken := &models.AuthToken{}
		if err := tx.Find(authToken, tokenID); err != nil {
			c.Logger().Error(err)
			return c.Render(http.StatusUnauthorized, r.Auto(c, Error{"Invalid token!"}))
		}

		nonceInt, err := strconv.Atoi(nonce)
		if err != nil {
			return c.Render(http.StatusBadRequest, r.Auto(c, Error{"Nonce is not an integer!"}))
		}

		if authToken.Nonce != nonceInt {
			// TODO: License could also be deleted to free it. Consider the implications.
			c.Logger().Warnf("Wrong nonce '%d' for token %s, expected: %d", nonceInt, tokenID, authToken.Nonce)
			if err := tx.Destroy(authToken); tx.TX.Commit() != nil || err != nil {
				return c.Render(http.StatusInternalServerError, r.Auto(c, Error{}))
			}
			return c.Render(http.StatusUnauthorized, r.Auto(c, Error{"Invalid nonce!"}))
		}

		authToken.Nonce = rand.Intn(900000000) + 100000000
		if err := tx.Update(authToken); err != nil {
			return c.Render(http.StatusInternalServerError, r.Auto(c, Error{}))
		}

		c.Response().Header().Set("X-Content-Nonce", strconv.Itoa(authToken.Nonce))
		return next(c)
	}
}
