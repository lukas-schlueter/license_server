package api

import (
	"encoding/json"
	"strconv"

	"github.com/gofrs/uuid"
	"github.com/o1egl/paseto"
	"gitlab.com/lukas-schlueter/license_server/models"
)

func (as *ActionSuite) Test_Api_Middleware_GetTokenFromHeader() {
	var tokenFromHeaderData = []struct {
		input  string
		output string
		err    error
	}{
		{"", "", ErrNoToken},
		{"Bearer", "", ErrTokenInvalid},
		{"Bearer ", "", ErrTokenInvalid},
		{"Bearer TestData", "TestData", nil},
		{"Bearer TestDataBearer ", "TestDataBearer", nil},
		{"Bearer TestDataBearer Bearer Test ", "TestDataBearer Bearer Test", nil},
	}

	for _, testData := range tokenFromHeaderData {
		output, err := getTokenFromHeader(testData.input)
		as.Equal(testData.output, output)

		if testData.err != nil {
			as.EqualError(err, testData.err.Error())
		} else {
			as.NoError(err)
		}
	}
}

func (as *ActionSuite) Test_Api_Middleware_Nonce_ValidRepetitions() {
	options := testTokenOptions{}
	token, nonce := as.getTestToken(options)
	as.IsType(int(0), nonce)

	repetitions := 20

	var oldNonce = nonce
	for i := 0; i < repetitions; i++ {
		res, newNonce := get(as.JSON("/sample"), token, oldNonce)
		as.Equal(200, res.Code)
		as.Contains(res.Body.String(), "Hello from api!")
		as.IsType(int(0), newNonce)
		as.NotEqual(oldNonce, newNonce)
		oldNonce = newNonce
	}
}

func (as *ActionSuite) Test_Api_Middleware_Nonce_Invalid() {
	var invalidNonceTestData = []struct {
		nonce        string
		code         int
		errorMessage string
		deleteToken  bool
	}{
		{"nonce", 400, "Nonce is not an integer!", false},
		{"", 400, "Nonce is not an integer!", false},
		{"12345", 401, "Invalid nonce!", true},
	}
	options := testTokenOptions{}

	for _, testData := range invalidNonceTestData {
		token, nonce := as.getTestToken(options)
		as.IsType(int(0), nonce)

		as.tokenExists(token)

		for strconv.Itoa(nonce) == testData.nonce {
			token, nonce = as.getTestToken(options)
			as.IsType(int(0), nonce)
		}

		req := as.JSON("/sample")
		req.Headers["Authorization"] = "Bearer " + token
		req.Headers["X-Content-Nonce"] = testData.nonce
		res := req.Get()

		as.Equal(testData.code, res.Code)

		newNonce := res.Header().Get("X-Content-Nonce")
		as.Empty(newNonce)

		response := &Error{}
		err := json.Unmarshal(res.Body.Bytes(), response)
		as.NoError(err)
		as.Equal(testData.errorMessage, response.Error)

		as.Equal(!testData.deleteToken, tokenExists(*as, token))
	}
}

func (as *ActionSuite) tokenExists(token string) {
	as.True(tokenExists(*as, token))
}
func (as *ActionSuite) tokenNotExists(token string) {
	as.False(tokenExists(*as, token))
}

func tokenExists(as ActionSuite, token string) bool {
	pasetoToken := paseto.JSONToken{}
	err := v2.Decrypt(token, authKey, &pasetoToken, nil)
	as.NoError(err)
	tokenUUID, err := uuid.FromString(pasetoToken.Jti)
	as.NoError(err)

	authToken := &models.AuthToken{}
	if err := as.DB.Find(authToken, tokenUUID); err != nil {
		return false
	}
	return true
}
