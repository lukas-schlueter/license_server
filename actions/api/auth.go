package api

// The errors, AuthenticateToken and getTokenFromHeader are strongly based on timeOr copied from the awesome gobuffalo/mw-tokenauth found here:
// https://github.com/gobuffalo/mw-tokenauth
// That code is licensed under The MIT License, Copyright (c) 2016 Mark Bates

import (
	"encoding/json"
	"fmt"
	"math/rand"
	"net/http"
	"time"

	"github.com/gobuffalo/buffalo"
	"github.com/gobuffalo/envy"
	"github.com/gobuffalo/pop"
	"github.com/gofrs/uuid"
	"github.com/o1egl/paseto"
	"github.com/pkg/errors"
	"gitlab.com/lukas-schlueter/license_server/models"
)

var (
	authKey []byte
	v2      = paseto.NewV2()
	// ErrTokenInvalid is returned when the token provided is invalid
	ErrTokenInvalid = errors.New("token invalid")
	// ErrNoToken is returned if no token is supplied in the request.
	ErrNoToken = errors.New("token not found in request")
	// FOOTER is the footer used in the Paseto token
	FOOTER = envy.Get("PASETO_FOOTER", "LicenseServer")
)

func parseGenerateTokenInput(payload *GenerateTokenPayload, tx *pop.Connection) (*models.License, *models.User, *models.Machine, *Error) {
	license := &models.License{}
	if err := tx.Eager().Find(license, payload.LicenseID); err != nil {
		return nil, nil, nil, &Error{"Invalid license!"}
	}

	user := &models.User{}
	if err := tx.Eager().Find(user, payload.UserID); err != nil {
		return nil, nil, nil, &Error{"Invalid user!"}
	}

	machine := &models.Machine{}
	if err := tx.Eager().Find(machine, payload.MachineID); err != nil {
		fmt.Println(err)
		return nil, nil, nil, &Error{"Invalid machine!"}
	}

	if license.User != *user || machine.User != *user || len(license.Machines) >= license.MachineLimit {
		return nil, nil, nil, &Error{"Invalid license!"}
	}
	return license, user, machine, nil
}

func createAuthToken(tx pop.Connection) (*models.AuthToken, error) {
	id, err := uuid.NewV4()
	if err != nil {
		return nil, err
	}

	nonce := rand.Intn(900000000) + 100000000
	authToken := &models.AuthToken{
		ID:    id,
		Nonce: nonce,
	}

	verrs, err := tx.ValidateAndCreate(authToken)
	if verrs.HasAny() || err != nil {
		return nil, err
	}

	return authToken, err
}

func createToken(license models.License, id uuid.UUID, now time.Time) (string, error) {
	token := paseto.JSONToken{
		IssuedAt:  now.Local(),
		NotBefore: now.Local(),
		// TODO Think about Expiration
		Expiration: now.Add(24 * time.Hour),
		Jti:        id.String(),
	}

	tokenPayload, err := json.Marshal(TokenPayload{LicenseID: license.ID})
	if err != nil {
		return "", err
	}
	token.Set("data", string(tokenPayload))

	encrypted, err := v2.Encrypt(authKey, token, FOOTER)
	if err != nil {
		return "", err
	}
	return encrypted, nil
}

// GenerateToken generates a new token for the user
func GenerateToken(c buffalo.Context) error {
	if !features.IsEnabled("api-token-generation") {
		return c.Error(http.StatusNotFound, errors.New("Token generation is not active"))
	}
	tx, ok := c.Value("tx").(*pop.Connection)
	if !ok {
		return errors.WithStack(errors.New("no transaction found"))
	}
	// Allocate an empty Machine
	payload := &GenerateTokenPayload{}

	// Bind machine to the html form elements
	if err := c.Bind(payload); err != nil {
		return c.Render(http.StatusBadRequest, r.Auto(c, Error{"Couldn't parse request."}))
	}

	license, _, machine, parseError := parseGenerateTokenInput(payload, tx)
	if parseError != nil {
		return c.Render(http.StatusUnauthorized, r.Auto(c, parseError))
	}

	for _, m := range license.Machines {
		if machine.ID == m.ID {
			return c.Render(http.StatusConflict, r.Auto(c, Error{"License already redeemed for this machine!"}))
		}
	}

	stmt := "INSERT INTO machines_licenses (machine_id, license_id, created_at, updated_at) SELECT ?,?,?,? WHERE NOT EXISTS (SELECT * FROM machines_licenses WHERE machine_id = ? AND license_id = ?)"
	if err := tx.RawQuery(stmt, machine.ID, license.ID, time.Now(), time.Now(), machine.ID, license.ID).Exec(); err != nil {
		c.Logger().Error(err)
		return c.Render(http.StatusInternalServerError, r.Auto(c, Error{"Failed to update license!"}))
	}

	authToken, err := createAuthToken(*tx)
	if err != nil {
		c.Logger().Error(err)
		return c.Render(http.StatusInternalServerError, r.Auto(c, Error{"Failed to create token!"}))
	}

	encrypted, err := createToken(*license, authToken.ID, authToken.CreatedAt)
	if err != nil {
		c.Logger().Error(err)
		return c.Render(http.StatusInternalServerError, r.Auto(c, Error{"Failed to create token!"}))

	}
	return c.Render(http.StatusCreated, r.Auto(c, GenerateTokenResult{Token: encrypted, Nonce: authToken.Nonce}))
}
