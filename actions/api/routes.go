package api

import (
	"github.com/gobuffalo/buffalo"
)

func mountAuthRoutes(group *buffalo.App) *buffalo.App {
	auth := group.Group("/auth")
	auth.POST("/token", GenerateToken)

	return auth
}

func mountSampleRoute(group *buffalo.App) *buffalo.App {
	group.GET("/sample", SampleHandler)
	return nil
}
