package api

import (
	"encoding/json"
)

func (as *ActionSuite) Test_TestHandler() {
	options := testTokenOptions{}
	token, nonce := as.getTestToken(options)

	res, _ := get(as.JSON("/sample"), token, nonce)
	println(res)
	as.Equal(200, res.Code)
	as.Contains(res.Body.String(), "Hello from api!")
}

func (as *ActionSuite) Test_TestHandlerJson() {
	require([]string{"api-sample-json"}, func() {
		options := testTokenOptions{}
		token, nonce := as.getTestToken(options)

		res, _ := get(as.JSON("/sample"), token, nonce)
		println(res)
		as.Equal(200, res.Code)
		returnedSample := &sample{}
		err := json.Unmarshal(res.Body.Bytes(), returnedSample)
		as.NoError(err)
		as.Equal("TestMsg", returnedSample.Msg)
	})
}
