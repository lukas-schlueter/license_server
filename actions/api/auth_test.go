package api

import (
	"encoding/json"

	"github.com/gofrs/uuid"
	. "gitlab.com/lukas-schlueter/license_server/models"
)

func (as *ActionSuite) Test_Api_GenerateToken() {
	authKey = []byte("TEST-KEY-TEST-KEY-TEST-KEY-TEST-")
	require([]string{"api-token-generation"}, func() {
		user, _, machine, license := setupSingleMachine(*as)

		payload := GenerateTokenPayload{UserID: user.ID, LicenseID: license.ID, MachineID: machine.ID}

		tokenCount, err := as.DB.Count(AuthToken{})
		as.NoError(err)
		as.Equal(0, tokenCount)

		res := as.JSON("/auth/token").Post(payload)
		as.Equal(201, res.Code)

		response := &GenerateTokenResult{}
		err = json.Unmarshal(res.Body.Bytes(), response)
		as.NoError(err)

		tokenCount, err = as.DB.Count(AuthToken{})
		as.NoError(err)
		as.Equal(1, tokenCount)
	})
}

func (as *ActionSuite) Test_Api_GenerateToken_Replay() {
	authKey = []byte("TEST-KEY-TEST-KEY-TEST-KEY-TEST-")
	require([]string{"api-token-generation"}, func() {
		user, _, machine, license := setupSingleMachine(*as)

		tokenCount, err := as.DB.Count(AuthToken{})
		as.NoError(err)
		as.Equal(0, tokenCount)

		license.MachineLimit = 2
		err = as.DB.Update(&license)
		as.NoError(err)

		payload := GenerateTokenPayload{UserID: user.ID, LicenseID: license.ID, MachineID: machine.ID}

		res := as.JSON("/auth/token").Post(payload)
		as.Equal(201, res.Code)

		response := &GenerateTokenResult{}
		err = json.Unmarshal(res.Body.Bytes(), response)
		as.NoError(err)

		tokenCount, err = as.DB.Count(AuthToken{})
		as.NoError(err)
		as.Equal(1, tokenCount)

		res = as.JSON("/auth/token").Post(payload)
		as.Equal(409, res.Code)

		resultError := &Error{}
		err = json.Unmarshal(res.Body.Bytes(), resultError)
		as.NoError(err)
		as.Equal("License already redeemed for this machine!", resultError.Error)

		tokenCount, err = as.DB.Count(AuthToken{})
		as.NoError(err)
		as.Equal(1, tokenCount)
	})
}

func (as *ActionSuite) Test_Api_GenerateToken_Repetition() {
	authKey = []byte("TEST-KEY-TEST-KEY-TEST-KEY-TEST-")
	require([]string{"api-token-generation"}, func() {
		user, _, machines, license := setupTwoMachines(*as)

		tokenCount, err := as.DB.Count(AuthToken{})
		as.NoError(err)
		as.Equal(0, tokenCount)

		payload := GenerateTokenPayload{UserID: user.ID, LicenseID: license.ID, MachineID: machines[0].ID}

		res := as.JSON("/auth/token").Post(payload)
		as.Equal(201, res.Code)

		response := &GenerateTokenResult{}
		err = json.Unmarshal(res.Body.Bytes(), response)
		as.NoError(err)

		tokenCount, err = as.DB.Count(AuthToken{})
		as.NoError(err)
		as.Equal(1, tokenCount)

		payload2 := GenerateTokenPayload{UserID: user.ID, LicenseID: license.ID, MachineID: machines[1].ID}

		res = as.JSON("/auth/token").Post(payload2)
		as.Equal(401, res.Code)

		resultError := &Error{}
		err = json.Unmarshal(res.Body.Bytes(), resultError)
		as.NoError(err)
		as.Equal("Invalid license!", resultError.Error)

		tokenCount, err = as.DB.Count(AuthToken{})
		as.NoError(err)
		as.Equal(1, tokenCount)
	})
}

func (as *ActionSuite) Test_Api_GenerateToken_InvalidPayloads() {
	authKey = []byte("TEST-KEY-TEST-KEY-TEST-KEY-TEST-")
	require([]string{"api-token-generation"}, func() {
		user, _, machine, license := setupSingleMachine(*as)

		var testCases = []struct {
			payload    GenerateTokenPayload
			statusCode int
			expected   string
		}{
			{GenerateTokenPayload{UserID: user.ID, MachineID: machine.ID}, 401, "Invalid license!"},
			{GenerateTokenPayload{LicenseID: license.ID, MachineID: machine.ID}, 401, "Invalid user!"},
			{GenerateTokenPayload{UserID: user.ID, LicenseID: license.ID}, 401, "Invalid machine!"},
		}

		tokenCount, err := as.DB.Count(AuthToken{})
		as.NoError(err)
		as.Equal(0, tokenCount)

		for _, testCase := range testCases {
			res := as.JSON("/auth/token").Post(testCase.payload)
			as.Equal(testCase.statusCode, res.Code)

			response := &Error{}
			err := json.Unmarshal(res.Body.Bytes(), response)
			as.NoError(err)

			as.Equal(testCase.expected, response.Error)

			tokenCount, err := as.DB.Count(AuthToken{})
			as.NoError(err)
			as.Equal(0, tokenCount)
		}

		tokenCount, err = as.DB.Count(AuthToken{})
		as.NoError(err)
		as.Equal(0, tokenCount)
	})
}

func setupSingleMachine(as ActionSuite) (User, Application, Machine, License) {
	u, a, m, l := loadFixture(as, "simple application setup single machine")

	user := u[0]
	application := a[0]
	machine := m[0]

	license := l[0]
	as.Equal(1, license.MachineLimit)
	as.Equal(0, len(license.Machines))

	return user, application, machine, license
}

func setupTwoMachines(as ActionSuite) (User, Application, Machines, License) {
	u, a, m, l := loadFixture(as, "simple application setup two machines")

	user := u[0]
	application := a[0]
	machines := m[0:2]

	license := l[0]
	as.Equal(1, license.MachineLimit)
	as.Equal(0, len(license.Machines))

	return user, application, machines, license
}

func loadFixture(as ActionSuite, name string) (Users, Applications, Machines, Licenses) {
	as.LoadFixture(name)

	users := &Users{}
	err := as.DB.All(users)
	as.NoError(err)

	var userIDs []uuid.UUID
	for _, user := range *users {
		userIDs = append(userIDs, user.ID)
	}

	applications := &Applications{}
	err = as.DB.All(applications)
	as.NoError(err)

	var appIDs []uuid.UUID
	for _, app := range *applications {
		appIDs = append(appIDs, app.ID)
		as.Contains(userIDs, app.UserID)
	}

	machines := &Machines{}
	err = as.DB.All(machines)
	as.NoError(err)

	var machineIDs []uuid.UUID
	for _, machine := range *machines {
		machineIDs = append(machineIDs, machine.ID)
		as.Contains(userIDs, machine.UserID)
	}

	licenses := &Licenses{}
	err = as.DB.All(licenses)
	as.NoError(err)

	var licenseIDs []uuid.UUID
	for _, license := range *licenses {
		licenseIDs = append(licenseIDs, license.ID)
		as.Contains(userIDs, license.UserID)
		as.Contains(appIDs, license.ApplicationID)
	}

	return *users, *applications, *machines, *licenses
}
