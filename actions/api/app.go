package api

import (
	"log"

	"github.com/gobuffalo/buffalo"
	"github.com/gobuffalo/buffalo-pop/pop/popmw"
	"github.com/gobuffalo/envy"
	"github.com/gobuffalo/x/sessions"
	feat "gitlab.com/lukas-schlueter/license_server/features"
	"gitlab.com/lukas-schlueter/license_server/models"
)

var features feat.FeatureProvider = feat.UnleashProvider{}

// App returns a new api group.
// It requires PASETO_KEY to be set.
func App() *buffalo.App {
	app := buffalo.New(buffalo.Options{
		Env:          envy.Get("GO_ENV", "development"),
		SessionStore: sessions.Null{},
	})
	api := app.Group("/")
	api.Use(popmw.Transaction(models.DB))

	api.Context = app.Context
	// Setup paseto authentication middleware
	key, err := envy.MustGet("PASETO_KEY")
	if err != nil {
		if envy.Get("GO_ENV", "development") != "test" {
			log.Fatal(app.Stop(err))
		}
		key = "TEST-KEY-TEST-KEY-TEST-KEY-TEST-"
	}
	auth := AuthenticateToken(Options{
		Key: []byte(key),
	})

	api.Use(auth)
	api.Use(AuthenticateNonce)
	api.Middleware.Skip(auth, GenerateToken)
	api.Middleware.Skip(AuthenticateNonce, GenerateToken)

	mountAuthRoutes(api)
	mountSampleRoute(api)

	return api
}
