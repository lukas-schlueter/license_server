package api

import (
	"encoding/json"
	"strconv"

	"github.com/gobuffalo/httptest"
	"gitlab.com/lukas-schlueter/license_server/models"
)

func (as *ActionSuite) TestApi_Token_Integration() {
	u, _, m, l := loadFixture(*as, "Single machine integration test")
	user := u[0]
	// application := a[0]
	machine := m[0]
	license := l[0]

	tokenResult := generateToken(*as, user, machine, license)

	requestSample(*as, tokenResult.Token, tokenResult.Nonce, 10)
}

func generateToken(as ActionSuite, user models.User, machine models.Machine, license models.License) *GenerateTokenResult {
	generateTokenPayload := GenerateTokenPayload{
		license.ID, user.ID, machine.ID,
	}

	var res *httptest.JSONResponse
	require([]string{"api-token-generation"}, func() {
		res = as.JSON("/auth/token").Post(generateTokenPayload)
		as.Equal(201, res.Code)
	})

	tokenResult := &GenerateTokenResult{}
	err := json.Unmarshal(res.Body.Bytes(), tokenResult)
	as.NoError(err)

	return tokenResult
}

func requestSample(as ActionSuite, token string, nonce int, iterations int) {
	for i := 0; i < iterations; i++ {
		req := as.JSON("/sample")
		req.Headers["Authorization"] = "Bearer " + token
		req.Headers["X-Content-Nonce"] = strconv.Itoa(nonce)

		var res *httptest.JSONResponse
		require([]string{"api-sample-json"}, func() {
			res = req.Get()
		})
		as.Equal(200, res.Code, res.Body.String())

		jsonResponse := &sample{}
		err := json.Unmarshal(res.Body.Bytes(), jsonResponse)
		as.NoError(err)
		as.Equal("TestMsg", jsonResponse.Msg)

		nonce, err = strconv.Atoi(res.Header().Get("X-Content-Nonce"))
		as.NoError(err)
	}
}
