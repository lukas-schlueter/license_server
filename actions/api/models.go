package api

import (
	"github.com/gobuffalo/uuid"
)

// Error is a generic error struct with an error message.
type Error struct {
	Error string `json:"error"`
}

// GenerateTokenPayload represents the expected json payload for token generation requests.
type GenerateTokenPayload struct {
	LicenseID uuid.UUID `json:"license_id"`
	UserID    uuid.UUID `json:"user_id"`
	MachineID uuid.UUID `json:"machine_id"`
}

// GenerateTokenResult represents the json returned from the token generation.
type GenerateTokenResult struct {
	// Token is a Paseto token bound to a license allowing access to an application.
	Token string `json:"token"`
	// Nonce is the initial nonce for the new api token.
	Nonce int `json:"nonce"`
}

// TokenPayload represents the encrypted json data included in each api token.
type TokenPayload struct {
	// LicenseID is the license this token is bound to.
	LicenseID uuid.UUID `json:"license_id"`
}
