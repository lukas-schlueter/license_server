package actions

import (
	"log"

	"github.com/Unleash/unleash-client-go"
	"github.com/casbin/casbin"
	"github.com/gobuffalo/buffalo"
	"github.com/gobuffalo/buffalo-pop/pop/popmw"
	"github.com/gobuffalo/envy"
	"github.com/gobuffalo/mw-csrf"
	"github.com/gobuffalo/mw-forcessl"
	"github.com/gobuffalo/mw-i18n"
	"github.com/gobuffalo/mw-paramlogger"
	"github.com/gobuffalo/packr"
	packr2 "github.com/gobuffalo/packr/v2"
	"github.com/unrolled/secure"
	"gitlab.com/lukas-schlueter/license_server/actions/middleware"
	feat "gitlab.com/lukas-schlueter/license_server/features"
	"gitlab.com/lukas-schlueter/license_server/models"
)

// ENV is used to help switch settings based on where the
// application is being run. Default is "development".
var ENV = envy.Get("GO_ENV", "development")
var app *buffalo.App
var features feat.FeatureProvider = feat.UnleashProvider{}
var enforcer *casbin.Enforcer

// T provides i18n support for the application
var T *i18n.Translator

func setupFeatureFlags() {
	if ENV == "test" {
		return
	}

	url, err := envy.MustGet("UNLEASH_URL")
	if err != nil {
		log.Fatal(app.Stop(err))
	}

	id, err := envy.MustGet("UNLEASH_INSTANCE_ID")
	if err != nil {
		log.Fatal(app.Stop(err))
	}

	name, err := envy.MustGet("UNLEASH_APP_NAME")
	if err != nil {
		log.Fatal(app.Stop(err))
	}

	err = features.Initialize(
		unleash.WithUrl(url),
		unleash.WithInstanceId(id),
		unleash.WithAppName(name),
		unleash.WithListener(unleash.DebugListener{}),
	)
	if err != nil {
		log.Fatal(app.Stop(err))
	}
}

// App is where all routes and middleware for buffalo
// should be defined. This is the nerve center of your
// application.
func App() *buffalo.App {
	if app == nil {
		app = buffalo.New(buffalo.Options{
			Env:         ENV,
			SessionName: "_license_server_session",
		})
		// Automatically redirect to SSL
		app.Use(forceSSL())

		if ENV == "development" {
			app.Use(paramlogger.ParameterLogger)
		}

		// Protect against CSRF attacks. https://www.owasp.org/index.php/Cross-Site_Request_Forgery_(CSRF)
		// Remove to disable this.
		app.Use(csrf.New)

		// Wraps each request in a transaction.
		//  c.Value("tx").(*pop.PopTransaction)
		// Remove to disable this.
		app.Use(popmw.Transaction(models.DB))

		// Setup and use translations:
		app.Use(translations())

		app.Use(SetCurrentUser)

		casbinConfig := &middleware.CasbinConfig{
			Connection:     models.DB,
			ConfigBox:      *packr2.New("ConfigBox", "../config"),
			Anonymous:      "guest",
			RedirectionURL: "/",
		}
		casbinMW, err := middleware.Casbin(casbinConfig)
		if err != nil {
			log.Fatal(app.Stop(err))
		}
		enforcer = casbinConfig.Enforcer
		app.Use(casbinMW)

		app.GET("/", HomeHandler)

		setupFeatureFlags()

		users := mountUserRoutes(app)
		mountMachines(users)

		applications := mountApplicationRoutes(app)
		mountLicenseRoutes(applications)

		mountAPI(app)

		app.ServeFiles("/", assetsBox) // serve files from the public directory

	}

	return app
}

// translations will load locale files, set up the translator `actions.T`,
// and will return a middleware to use to load the correct locale for each
// request.
// for more information: https://gobuffalo.io/en/docs/localization
func translations() buffalo.MiddlewareFunc {
	var err error
	if T, err = i18n.New(packr.NewBox("../locales"), "en-US"); err != nil {
		log.Fatal(app.Stop(err))
	}
	return T.Middleware()
}

// forceSSL will return a middleware that will redirect an incoming request
// if it is not HTTPS. "http://example.com" => "https://example.com".
// This middleware does **not** enable SSL. for your application. To do that
// we recommend using a proxy: https://gobuffalo.io/en/docs/proxy
// for more information: https://github.com/unrolled/secure/
func forceSSL() buffalo.MiddlewareFunc {
	return forcessl.Middleware(secure.Options{
		SSLRedirect:     ENV == "production",
		SSLProxyHeaders: map[string]string{"X-Forwarded-Proto": "https"},
		IsDevelopment:   ENV != "production",
	})
}
