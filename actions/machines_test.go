package actions

import (
	"encoding/json"

	"gitlab.com/lukas-schlueter/license_server/models"
)

func (as *ActionSuite) Test_MachinesResource_List() {
	u, machine := setupWithMachineWithLicense(as)

	res := as.JSON("/users/%s/machines", u.ID).Get()
	as.Equal(200, res.Code)
	returnedMachines := &models.Machines{}
	err := json.Unmarshal(res.Body.Bytes(), returnedMachines)
	as.NoError(err)
	as.Equal(&models.Machines{*machine}, returnedMachines)
}

func (as *ActionSuite) Test_MachinesResource_Show() {
	u, machine := setupWithMachineWithLicense(as)

	res := as.JSON("/users/%s/machines/%s", u.ID, machine.ID).Get()
	as.Equal(200, res.Code)
	returnedMachines := &models.Machine{}
	err := json.Unmarshal(res.Body.Bytes(), returnedMachines)
	as.NoError(err)
	as.Equal(machine, returnedMachines)
}

func (as *ActionSuite) Test_MachinesResource_New() {
	// as.Fail("Not Implemented!")
}

func (as *ActionSuite) Test_MachinesResource_Create() {
	// as.Fail("Not Implemented!")
}

func (as *ActionSuite) Test_MachinesResource_Edit() {
	// as.Fail("Not Implemented!")
}

func (as *ActionSuite) Test_MachinesResource_Update() {
	// as.Fail("Not Implemented!")
}

func (as *ActionSuite) Test_MachinesResource_Destroy() {
	// as.Fail("Not Implemented!")
}

func setupNoMachine(as *ActionSuite) *models.User {
	u := login(as, "no machines")
	return u
}

func setupWithMachine(as *ActionSuite) (*models.User, *models.Machine) {
	u := login(as, "single machine")

	a := &models.Machine{}
	err := as.DB.Eager().First(a)
	as.NoError(err)
	as.NotNil(a.UserID)
	as.Equal(u.ID, a.UserID)

	return u, a
}

func setupWithMachineWithLicense(as *ActionSuite) (*models.User, *models.Machine) {
	u := login(as, "single machine with license")

	a := &models.Machine{}
	err := as.DB.Eager().First(a)
	as.NoError(err)
	as.NotNil(a.UserID)
	as.Equal(u.ID, a.UserID)

	return u, a
}
