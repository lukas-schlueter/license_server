package actions

import (
	"encoding/json"

	"gitlab.com/lukas-schlueter/license_server/models"
)

func (as *ActionSuite) Test_ApplicationsResource_List() {
	_, a := setupWithApplication(as)

	res := as.JSON("/applications").Get()
	as.Equal(200, res.Code)
	returnedApplications := &models.Applications{}
	err := json.Unmarshal(res.Body.Bytes(), returnedApplications)
	as.NoError(err)
	as.Equal(&models.Applications{*a}, returnedApplications)
}

func (as *ActionSuite) Test_ApplicationsResource_Show() {
	_, a := setupWithApplication(as)

	res := as.JSON("/applications/%s", a.ID).Get()
	as.Equal(200, res.Code)
	returnedApplications := &models.Application{}
	err := json.Unmarshal(res.Body.Bytes(), returnedApplications)
	as.NoError(err)
	as.Equal(a, returnedApplications)
}

func (as *ActionSuite) Test_ApplicationsResource_New() {
	setupNoApplication(as)
	res := as.HTML("/applications/new").Get()
	as.Equal(200, res.Code)
}

func (as *ActionSuite) Test_ApplicationsResource_Create() {
	u := setupNoApplication(as)

	count, err := as.DB.Count("applications")
	as.NoError(err)
	as.Equal(0, count)

	a := &models.Application{Name: "TestApp"}

	res := as.JSON("/applications").Post(a)
	as.Equal(201, res.Code)

	returnedApplication := &models.Application{}
	err = json.Unmarshal(res.Body.Bytes(), returnedApplication)
	as.NoError(err)
	as.Equal(a.Name, returnedApplication.Name)
	as.Equal(u.ID, returnedApplication.UserID)

	// Applications without a name should not be accepted
	a = &models.Application{}
	res = as.JSON("/applications").Post(a)
	as.Equal(422, res.Code)
}

func (as *ActionSuite) Test_ApplicationsResource_Edit() {
	_, a := setupWithApplication(as)

	res := as.JSON("/applications/%s/edit", a.ID).Get()
	as.Equal(200, res.Code)

	returnedApplications := &models.Application{}
	err := json.Unmarshal(res.Body.Bytes(), returnedApplications)
	as.NoError(err)
	as.Equal(a, returnedApplications)
}

func (as *ActionSuite) Test_ApplicationsResource_Update() {
	_, a := setupWithApplication(as)

	a2 := a
	a2.Name = a.Name + "_v2.0.0"

	res := as.JSON("/applications/%s", a.ID).Put(a2)
	as.Equal(200, res.Code)

	returnedApplication := &models.Application{}
	err := json.Unmarshal(res.Body.Bytes(), returnedApplication)
	as.NoError(err)
	as.NotEqual(a, returnedApplication)
	as.NotEqual(a2.UpdatedAt, returnedApplication.UpdatedAt)

	// All fields except for UpdatedAt should be the same
	a2.UpdatedAt = returnedApplication.UpdatedAt
	as.Equal(a2, returnedApplication)

	// Applications without a name should not be accepted
	a3 := &models.Application{}
	res = as.JSON("/applications/%s", a.ID).Put(a3)
	as.Equal(422, res.Code)
}

func (as *ActionSuite) Test_ApplicationsResource_Destroy() {
	_, a := setupWithApplication(as)

	count, err := as.DB.Count("applications")
	as.NoError(err)
	as.Equal(1, count)

	res := as.JSON("/applications/%s", a.ID).Delete()
	as.Equal(200, res.Code)

	count, err = as.DB.Count("applications")
	as.NoError(err)
	as.Equal(0, count)
}

func setupNoApplication(as *ActionSuite) *models.User {
	u := login(as, "no applications")
	return u
}

func setupWithApplication(as *ActionSuite) (*models.User, *models.Application) {
	u := login(as, "single application")

	a := &models.Application{}
	err := as.DB.Eager().First(a)
	as.NoError(err)
	as.NotNil(a.UserID)
	as.Equal(u.ID, a.UserID)

	return u, a
}
