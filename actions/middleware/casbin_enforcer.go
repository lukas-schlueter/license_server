package middleware

import (
	"fmt"
	"log"
	"net/http"
	"strings"

	"github.com/casbin/casbin"
	"github.com/casbin/gorm-adapter"
	"github.com/gobuffalo/buffalo"
	"github.com/gobuffalo/packr/v2"
	"github.com/gobuffalo/pop"
)

// CasbinConfig provides configuration information for the casbin middleware.
type CasbinConfig struct {
	// Connection provides a database connection.
	Connection *pop.Connection
	// Anonymous defines the default "group" for users that are not logged in.
	Anonymous string
	// RedirectionURL is the location to which an denied access should redirect.
	RedirectionURL string
	// ConfigBox provides access to the casbin configuration files.
	ConfigBox packr.Box
	// Enforcer will be set by NewCasbinEnforcer.
	Enforcer *casbin.Enforcer
}

// Casbin creates a new casbin enforcer for the provided config and
// returns a permission checking middleware.
func Casbin(config *CasbinConfig) (buffalo.MiddlewareFunc, error) {
	err := NewCasbinEnforcer(config)
	if err != nil {
		return nil, err
	}

	return func(next buffalo.Handler) buffalo.Handler {
		return func(c buffalo.Context) error {
			if config.checkPermission(c) {
				return next(c)
			}
			c.Flash().Add("danger", "You must be authorized to see that page!")
			return c.Redirect(http.StatusForbidden, config.RedirectionURL)
		}
	}, nil
}

func (config *CasbinConfig) getUserName(c buffalo.Context) string {
	u := c.Session().Get("user_id")

	if user, ok := u.(string); u != nil && ok {
		return user
	}
	return config.Anonymous
}

func (config *CasbinConfig) checkPermission(c buffalo.Context) bool {
	user := config.getUserName(c)
	method := c.Request().Method
	path := c.Request().URL.Path
	fmt.Println(user, path, method)
	fmt.Println(config.Enforcer.GetAllRoles())
	return config.Enforcer.Enforce(user, path, method)
}

// InitializeRoles reads roles.csv from the config box
// and applies the permission definitions (casbin policy).
func (config *CasbinConfig) InitializeRoles() {
	roles, err := config.ConfigBox.FindString("roles.csv")
	if err != nil {
		log.Println(err)
		return
	}
	lines := strings.Split(roles, "\n")
	for _, line := range lines {
		line = strings.TrimSpace(line)
		if len(line) == 0 {
			continue
		}
		args := strings.Split(line, ",")
		ptype := args[0]
		args = args[1:]
		for i := range args {
			args[i] = strings.TrimSpace(args[i])
		}
		fmt.Println(args)
		if ptype == "g" {
			config.Enforcer.AddRoleForUser(args[0], args[1])
		} else {
			config.Enforcer.AddPermissionForUser(args[0], args[1:]...)
		}
	}
}

// NewCasbinEnforcer creates and sets a new casbin enforcer for the
// provided Config. A gorm adapter is used for storage.
// `model.conf` from the config box is used for model configuration.
func NewCasbinEnforcer(config *CasbinConfig) error {
	adapter := gormadapter.NewAdapter(config.Connection.Dialect.Name(), buildDBString(*config.Connection.Dialect.Details()), true)

	model, err := config.ConfigBox.FindString("model.conf")
	if err != nil {
		return err
	}
	config.Enforcer = casbin.NewEnforcer(casbin.NewModel(model), adapter)

	config.InitializeRoles()
	return nil
}

func buildDBString(con pop.ConnectionDetails) string {
	return fmt.Sprintf(
		"dbname=%s user=%s password=%s host=%s port=%s sslmode=%s", con.Database, con.User, con.Password, con.Host, con.Port, con.Options["sslmode"],
	)
}
