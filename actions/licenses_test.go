package actions

import (
	"encoding/json"

	"gitlab.com/lukas-schlueter/license_server/models"
)

func (as *ActionSuite) Test_LicensesResource_List() {
	_, a, l := setupWithLicense(as)

	res := as.JSON("/applications/%s/licenses", a.ID).Get()
	as.Equal(200, res.Code)
	returnedLicenses := &models.Licenses{}
	err := json.Unmarshal(res.Body.Bytes(), returnedLicenses)
	as.NoError(err)
	as.Equal(&models.Licenses{*l}, returnedLicenses)
}

func (as *ActionSuite) Test_LicensesResource_Show() {
	_, a, l := setupWithLicense(as)

	res := as.JSON("/applications/%s/licenses/%s", a.ID, l.ID.String()).Get()
	as.Equal(200, res.Code)
	returnedLicense := &models.License{}
	err := json.Unmarshal(res.Body.Bytes(), returnedLicense)
	as.NoError(err)
	as.Equal(l, returnedLicense)
}

func (as *ActionSuite) Test_LicensesResource_New() {
	_, a := setupNoLicense(as)
	res := as.HTML("/applications/%s/licenses/new", a.ID).Get()
	as.Equal(200, res.Code)

}

func (as *ActionSuite) Test_LicensesResource_Create() {
	u, a := setupNoLicense(as)

	count, err := as.DB.Count("licenses")
	as.NoError(err)
	as.Equal(0, count)

	l := &models.License{MachineLimit: 5}

	res := as.JSON("/applications/%s/licenses", a.ID).Post(l)
	as.Equal(201, res.Code)

	returnedLicense := &models.License{}
	err = json.Unmarshal(res.Body.Bytes(), returnedLicense)
	as.NoError(err)
	as.Equal(l.MachineLimit, returnedLicense.MachineLimit)
	as.Equal(u.ID, returnedLicense.UserID)
	as.Equal(*u, returnedLicense.User)

	// Licenses without fields should not be accepted
	l = &models.License{}
	res = as.JSON("/applications/%s/licenses", a.ID).Post(l)
	as.Equal(422, res.Code)
}

func (as *ActionSuite) Test_LicensesResource_Edit() {
	_, a, l := setupWithLicense(as)

	res := as.JSON("/applications/%s/licenses/%s/edit", a.ID, l.ID).Get()
	as.Equal(200, res.Code)

	returnedLicense := &models.License{}
	err := json.Unmarshal(res.Body.Bytes(), returnedLicense)
	as.NoError(err)
	as.Equal(l, returnedLicense)
}

func (as *ActionSuite) Test_LicensesResource_Update() {
	_, a, l := setupWithLicense(as)

	l2 := l
	l2.MachineLimit = l.MachineLimit + 5

	res := as.JSON("/applications/%s/licenses/%s", a.ID, l.ID.String()).Put(l2)
	as.Equal(200, res.Code)

	returnedLicense := &models.License{}
	err := json.Unmarshal(res.Body.Bytes(), returnedLicense)
	as.NoError(err)
	as.NotEqual(l, returnedLicense)
	as.NotEqual(l2.UpdatedAt, returnedLicense.UpdatedAt)

	// All fields except for UpdatedAt should be the same
	l2.UpdatedAt = returnedLicense.UpdatedAt
	as.Equal(l2, returnedLicense)

	// Licenses without fields should not be accepted
	l3 := &models.License{}
	res = as.JSON("/applications/%s/licenses/%s", a.ID, l.ID.String()).Put(l3)
	as.Equal(422, res.Code)
}

func (as *ActionSuite) Test_LicensesResource_Destroy() {
	_, a, l := setupWithLicense(as)

	count, err := as.DB.Count("licenses")
	as.NoError(err)
	as.Equal(1, count)

	res := as.JSON("/applications/%s/licenses/%s", a.ID, l.ID).Delete()
	as.Equal(200, res.Code)

	count, err = as.DB.Count("licenses")
	as.NoError(err)
	as.Equal(0, count)
}

func setupNoLicense(as *ActionSuite) (*models.User, *models.Application) {
	u := login(as, "no licenses")

	a := &models.Application{}
	err := as.DB.Eager().First(a)
	as.NoError(err)
	return u, a
}

func setupWithLicense(as *ActionSuite) (*models.User, *models.Application, *models.License) {
	u := login(as, "single license")

	l := &models.License{}
	err := as.DB.Eager().First(l)
	as.NoError(err)
	as.NotNil(l.User)
	as.Equal(*u, l.User)

	return u, &l.Application, l
}
