package actions

import (
	"fmt"
	"regexp"

	"github.com/stanislas-m/mocksmtp"
	"gitlab.com/lukas-schlueter/license_server/mailers"
	"gitlab.com/lukas-schlueter/license_server/models"
)

func (as *ActionSuite) Test_UsersResource_Show() {
	u := login(as, "single user")

	res := as.HTML(fmt.Sprintf("/users/%s", u.ID)).Get()
	as.Equal(200, res.Code)
	as.Contains(res.Body.String(), u.ID.String())
	as.Contains(res.Body.String(), u.Email)
}

func (as *ActionSuite) Test_UsersResource_New() {
	res := as.HTML("/users/new").Get()
	as.Equal(200, res.Code)
}

func (as *ActionSuite) Test_UsersResource_Create() *models.User {
	ms, v := mailers.SMTP.(*mocksmtp.MockSMTP)
	as.True(v)
	defer ms.Clear()

	count, err := as.DB.Count("users")
	as.NoError(err)
	as.Equal(0, count)

	count, err = as.DB.Count("tokens")
	as.NoError(err)
	as.Equal(0, count)

	u := &models.User{Email: "test@example.com"}

	res := as.HTML("/users").Post(u)
	as.Equal(303, res.Code)
	res = as.HTML(res.Location()).Get()
	as.Equal(200, res.Code)
	as.Contains(res.Body.String(), "Please confirm the login with your email")

	count, err = as.DB.Count("users")
	as.NoError(err)
	as.Equal(1, count)

	count, err = as.DB.Count("tokens")
	as.NoError(err)
	as.Equal(1, count)

	err = as.DB.First(u)
	as.NoError(err)
	t := &models.Token{}
	err = as.DB.First(t)
	as.NoError(err)

	as.Equal(u.ID, t.UserID)

	m, err := ms.LastMessage()
	fmt.Println(m)
	fmt.Println(m.Bodies[0].Content)

	as.NoError(err)
	as.Equal(1, len(m.To))
	as.Equal(u.Email, m.To[0])

	text := m.Bodies[0].Content
	as.Contains(text, u.ID.String())
	as.Contains(text, t.ID.String())

	re := regexp.MustCompile("href='https?://[^/]+([^']+)'")
	url := re.FindStringSubmatch(text)[1]

	fmt.Println(url)
	res = as.HTML(url).Get()
	as.Equal(302, res.Code)
	res = as.HTML(res.Location()).Get()
	as.Equal(200, res.Code)
	as.Contains(res.Body.String(), "success")

	count, err = as.DB.Count("tokens")
	as.NoError(err)
	as.Equal(0, count)

	return u
}

func (as *ActionSuite) Test_UsersResource_Edit() {
	u := login(as, "single user")

	res := as.HTML(fmt.Sprintf("/users/%s/edit", u.ID)).Get()
	as.Equal(200, res.Code)
	as.Contains(res.Body.String(), u.ID.String())
	as.Contains(res.Body.String(), u.Email)
}

func (as *ActionSuite) Test_UsersResource_Update() {
	u := login(as, "single user")

	u2 := models.User{ID: u.ID, Email: "updated@example.com"}

	res := as.HTML(fmt.Sprintf("/users/%s", u.ID)).Put(u2)
	as.Equal(302, res.Code)

	res = as.HTML(res.Location()).Get()
	as.Equal(200, res.Code)
	as.Contains(res.Body.String(), u.ID.String())
	as.Contains(res.Body.String(), u2.Email)
}

func (as *ActionSuite) Test_UsersResource_Destroy() {
	u := login(as, "single user")

	count, err := as.DB.Count("users")
	as.NoError(err)
	as.Equal(1, count)

	res := as.HTML(fmt.Sprintf("/users/%s", u.ID)).Delete()
	as.Equal(302, res.Code)

	res = as.HTML(res.Location()).Get()
	as.Equal(200, res.Code)

	count, err = as.DB.Count("users")
	as.NoError(err)
	as.Equal(0, count)
}

func (as *ActionSuite) Test_UsersResource_Logout() {
	u := login(as, "single user")

	res := as.HTML(fmt.Sprintf("/users/%s", u.ID)).Get()
	as.Equal(200, res.Code)

	res = as.HTML("/users/%s/logout", u.ID).Delete()
	as.Equal(302, res.Code)
	res = as.HTML(res.Location()).Get()
	as.Equal(200, res.Code)
	as.Contains(res.Body.String(), "You are now logged out!")

	res = as.HTML(fmt.Sprintf("/users/%s", u.ID)).Get()
	as.Equal(403, res.Code)
	res = as.HTML(res.Location()).Get()
	as.Equal(200, res.Code)
	as.Contains(res.Body.String(), "You must be authorized to see that page!")
}
