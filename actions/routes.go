package actions

import (
	"log"

	"github.com/gobuffalo/buffalo"
	"gitlab.com/lukas-schlueter/license_server/actions/api"
)

func mountUserRoutes(group *buffalo.App) (users *buffalo.App) {
	usersResource := UsersResource{}
	users = group.Resource("/users", usersResource)
	users.GET("/confirm", usersResource.Confirm).Name("confirmPath")
	users.DELETE("/logout", usersResource.Logout).Name("logoutPath")
	users.Use(AuthorizeOwnUser)
	users.Middleware.Skip(AuthorizeOwnUser, usersResource.List, usersResource.New, usersResource.Create, usersResource.Confirm)

	// Add "loginPath" as an alias to "newUsersPath"
	route, err := users.Routes().Lookup("newUsersPath")
	if err != nil {
		log.Fatal(users.Stop(err))
	}
	route.Name("loginPath")
	return
}

func mountApplicationRoutes(group *buffalo.App) (applications *buffalo.App) {
	applications = group.Resource("/applications", ApplicationsResource{})
	applications.Use(AuthorizeOwnUser)
	applications.Use(SetCurrentApplication)
	return
}

func mountLicenseRoutes(group *buffalo.App) (licenses *buffalo.App) {
	licenses = group.Resource("/licenses", LicensesResource{})
	return
}

func mountMachines(group *buffalo.App) (machines *buffalo.App) {
	machines = group.Resource("/machines", MachinesResource{})
	return
}

func mountAPI(group *buffalo.App) (app *buffalo.App) {
	app = api.App()
	group = group.Group("/api")
	group.Middleware.Clear()
	group.Mount("/", api.App())
	return
}
