package actions

import (
	"testing"

	"github.com/gobuffalo/packr"
	"github.com/gobuffalo/suite"
	"github.com/google/go-cmp/cmp"
	"gitlab.com/lukas-schlueter/license_server/models"
)

type ActionSuite struct {
	*suite.Action
}

func Test_ActionSuite(t *testing.T) {
	action, err := suite.NewActionWithFixtures(App(), packr.NewBox("../fixtures"))
	if err != nil {
		t.Fatal(err)
	}

	as := &ActionSuite{
		Action: action,
	}
	suite.Run(t, as)
}

func (as ActionSuite) Equal(expected, actual interface{}, opts ...cmp.Option) {
	as.True(cmp.Equal(expected, actual, opts...))
}

func login(as *ActionSuite, fixtureName string) *models.User {
	as.LoadFixture(fixtureName)
	u := &models.User{}
	err := as.DB.First(u)
	as.NoError(err)
	as.Session.Set("user_id", u.ID.String())
	enforcer.AddRoleForUser(u.ID.String(), "member")

	return u
}
