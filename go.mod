module gitlab.com/lukas-schlueter/license_server

require (
	cloud.google.com/go v0.36.0 // indirect
	github.com/Unleash/unleash-client-go v0.0.0-20181121205122-ae068e0ad68c
	github.com/casbin/casbin v1.8.1
	github.com/casbin/gorm-adapter v0.0.0-20190103152927-5f0f4b5ecad7
	github.com/denisenkom/go-mssqldb v0.0.0-20190204142019-df6d76eb9289 // indirect
	github.com/erikstmartin/go-testdb v0.0.0-20160219214506-8d10e4a1bae5 // indirect
	github.com/gobuffalo/buffalo v0.13.13
	github.com/gobuffalo/buffalo-pop v1.6.0
	github.com/gobuffalo/envy v1.6.12
	github.com/gobuffalo/genny v0.0.0-20190131190646-008a76242145 // indirect
	github.com/gobuffalo/httptest v1.0.6
	github.com/gobuffalo/meta v0.0.0-20190126124307-c8fb6f4eb5a9 // indirect
	github.com/gobuffalo/mw-csrf v0.0.0-20190129204204-25460a055517
	github.com/gobuffalo/mw-forcessl v0.0.0-20180802152810-73921ae7a130
	github.com/gobuffalo/mw-i18n v0.0.0-20190129204410-552713a3ebb4
	github.com/gobuffalo/mw-paramlogger v0.0.0-20190129202837-395da1998525
	github.com/gobuffalo/packr v1.22.0
	github.com/gobuffalo/packr/v2 v2.0.0
	github.com/gobuffalo/pop v4.9.6+incompatible
	github.com/gobuffalo/suite v2.6.0+incompatible
	github.com/gobuffalo/uuid v2.0.5+incompatible
	github.com/gobuffalo/validate v2.0.3+incompatible
	github.com/gobuffalo/x v0.0.0-20181110221217-14085ca3e1a9
	github.com/gofrs/uuid v3.2.0+incompatible
	github.com/google/go-cmp v0.2.0
	github.com/jackc/pgx v3.3.0+incompatible // indirect
	github.com/jinzhu/gorm v1.9.2 // indirect
	github.com/jinzhu/inflection v0.0.0-20180308033659-04140366298a // indirect
	github.com/jinzhu/now v0.0.0-20181116074157-8ec929ed50c3 // indirect
	github.com/markbates/grift v1.0.5
	github.com/mitchellh/go-homedir v1.1.0 // indirect
	github.com/o1egl/paseto v1.0.0
	github.com/pkg/errors v0.8.1
	github.com/stanislas-m/mocksmtp v1.0.0
	github.com/unrolled/secure v0.0.0-20190103195806-76e6d4e9b90c
	golang.org/x/crypto v0.0.0-20190131182504-b8fe1690c613 // indirect
	golang.org/x/net v0.0.0-20190125091013-d26f9f9a57f3 // indirect
	golang.org/x/sys v0.0.0-20190201073406-2970a3744de3 // indirect
	golang.org/x/tools v0.0.0-20190131163942-067a2f313b69 // indirect
	gopkg.in/h2non/gock.v1 v1.0.14 // indirect
)
