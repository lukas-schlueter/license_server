package features

import (
	"github.com/Unleash/unleash-client-go"
)

// FeatureProvider handles feature toggles
type FeatureProvider interface {
	// Initialize performs setup operations and should only be called once
	Initialize(options ...unleash.ConfigOption) error
	// IsEnabled checks if a feature is enabled
	IsEnabled(feature string, options ...unleash.FeatureOption) bool
}

// UnleashProvider uses unleash for feature toggles.
// Implements FeatureProvider
type UnleashProvider struct {
}

// IsEnabled contacts the unleash server and checks if a feature is enabled
func (UnleashProvider) IsEnabled(feature string, options ...unleash.FeatureOption) bool {
	return unleash.IsEnabled(feature, options...)
}

// Initialize performs setup operations for unleash and should only be called once
func (UnleashProvider) Initialize(options ...unleash.ConfigOption) error {
	return unleash.Initialize(options...)
}

// MockProvider is an offline mock implementation for tests using a simple map[string]bool.
// Implements FeatureProvider.
type MockProvider struct {
	features map[string]bool
}

// Initialize creates a new empty feature map[string]bool
func (m *MockProvider) Initialize(options ...unleash.ConfigOption) error {
	m.features = make(map[string]bool)
	return nil
}

// IsEnabled checks if the feature is enabled in the map
func (m MockProvider) IsEnabled(feature string, options ...unleash.FeatureOption) bool {
	return m.features[feature]
}

// SetEnabled enables a feature for this instance
func (m *MockProvider) SetEnabled(feature string, enabled bool) {
	m.features[feature] = enabled
}
